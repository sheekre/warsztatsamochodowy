<?php

require_once "config.php";

$error = '';
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])) {

    $firstname = trim($_POST['firstname']);
    $lastname = trim($_POST['lastname']);
    $phone = trim($_POST['phone']);
    $email = trim($_POST['email']);
    $car = trim($_POST['car']);
    $model = trim($_POST['model']);
    $year = trim($_POST['year']);
    $engine = trim($_POST['engine']);
    $milage = trim($_POST['milage']);
    $question = trim($_POST['question']);
    $information = trim($_POST['information']);
    $acceptprivacy = $_POST['acceptprivacy'];
    $acceptprivacyint = intval($acceptprivacy);

    $query = $db->prepare("INSERT INTO inquiries (`firstname`, `lastname`, `phone`, `email`, `acceptprivacy`, `car`, `model`, `year`, `engine`, `milage`, `question`, `information`) 
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
    
    $query->bind_param('ssisissisiss',$firstname, $lastname, $phone, $email, $acceptprivacyint, $car, $model, $year, $engine, $milage, $question, $information);
    $result = $query->execute();

    if ($result) {
        $error .= '<p class="success">Wysłano wiadomość.</p>';
    } else {
        $error .= '<p class="error">Nie udało się wysłać wiadomości.</p>';
    }
}
?>

<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Warsztat Samochodowy</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/styles.css">

</head>

<body>

<ul class="navigation">
        <li><a href="mainpage.html">Strona Główna</a></li>
        <li><a href="about.html">O nas</a></li>
        <li><a href="offer.html">Oferta</a></li>
        <li><a href="contact.php">Kontakt</a></li>
        <li><a href="login.php">Login</a></li>
</ul>
<div class="container" role="main">
        <div class="my-bg-image">
	        <a href="mainpage.html">
            <h1 id="buzz-word">
		    Amazing Cars Factory<br>
		    Detailing & Tuning
            </h1>
	        </a>
        </div>
</div>

<div class="container">      
<div class="row">               
<div class="col-xl-12">
        <div>
            <br><br><br>
            <p>Witaj w Amazing Cars Factory: Detailing & Tuning. Jeśli szukasz profesjonalnego centrum tuningu samochodów, mechanika i warsztatu, który zaopiekuje się Twoim autem - zgłoś się do nas. Nasze usługi obejmują chiptuning, tuning mechaniczny i optyczny, sportowe układy wydechowe, aktywny układ wydechowy oraz mechanikę i diagnostykę komputerową. Co ważne, diagnostyka obejmuje również pojazdy ciężarowe i maszyny budowlane, która skutecznie rozprawia się z takimi usterkami jak problemy z DPF, EGR i AdBlue. Posiadamy również własną hamownię 4x4 i warsztat samochodowy. Odezwij się do nas.</p>
        </div>
            <form action="" method="post">
                <div>
                    <br><br>
                    <h3 class="font-weight-bold">Dane kontaktowe</h3>
                    <br>
                </div>

				<div class="form-group">
                    <label>Imię</label>
                    <input type="text" name="firstname" class="form-control" required>
                </div>

                <div class="form-group">
				    <label>Nazwisko</label> 
                    <input type="text" name="lastname" class="form-control" required>
                </div>

                <div class="form-group">
				    <label>Numer telefonu</label> 
                    <input type="number" name="phone" class="form-control" required>
                </div>

                <div class="form-group">
				    <label>Email</label> 
                    <input type="email" name="email" class="form-control" required>
                </div>

                <div>
                    <br><br>
                    <h3 class="font-weight-bold">Dane pojazdu</h3>
                    <br>
                </div>

                <div class="form-group">
				    <label>Marka</label> 
                    <input type="text" name="car" class="form-control" required>
                </div>

                <div class="form-group">
				    <label>Model</label> 
                    <input type="text" name="model" class="form-control" required>
                </div>

                <div class="form-group">
				    <label>Rocznik pojazdu</label> 
                    <input type="number" name="year" class="form-control" required>
                </div>

                <div class="form-group">
				    <label>Pojemność silnika</label> 
                    <input type="text" name="engine" class="form-control" required>
                </div>

                <div class="form-group">
				    <label>Przebieg</label> 
                    <input type="number" name="milage" class="form-control" required>
                </div>

                <div>
                    <br><br>
                    <h3 class="font-weight-bold">Opis problemu</h3>
                    <br>
                </div>

                <div class="form-group">
				    <label>Temat</label>
                    <input type="text" name="question" class="form-control" required>
                </div>

                <div class="form-group">
                    <label for="description">Opis</label>
                    <textarea class="form-control" name="information" rows="3"></textarea>
                </div>

                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="acceptprivacy" name="acceptprivacy">
                    <label class="form-check-label" for="acceptprivacy">Akceptuję warunki Polityki Prywatności</label>
                </div>

                <br><br>

                <div class="form-group">
                    <input type="submit" name="submit" class="btn btn-success" value="Wyślij">
				    <input type="reset" name="reset" class="btn btn-danger" value="Wyczyść">
                </div>
            </form>

    <div>
        <div>
            <footer>
                <li><a href="about.html">O nas</a></li>
                <li><a href="offer.html">Oferta</a></li>
                <li><a href="contact.php">Kontakt</a></li>
                <li><a href="login.php">Login</a></li>
            </footer>
        </div>       
    </div>
    <p id="footer">Szablon strony stworzony przez <a href="">Mateusz Bochnia i Tomasz Sędłak</a></p> 
</div>
</div>
</div>	
</body>
</html>