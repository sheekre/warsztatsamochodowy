<?php

require_once "config.php";
require_once "session.php";


$error = '';
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])) {

    $username = trim($_POST['username']);
    $inputpassword = trim($_POST['password'] ?? '');

    if (empty($username)) {
        $error .= '<p class="error">Proszę podać nazwę użytkownika.</p>';
    }

    if (empty($inputpassword)) {
        $error .= '<p class="error">Proszę podać hasło.</p>';
    }

    $query = $db->prepare("SELECT `Username`, `Password` FROM UserProfiles WHERE username = ?");

    if (empty($error)) {
        if($query != null) {
            $query->bind_param('s', $username);
            $query->bind_result($username, $password);
            $query->execute();
            $row = $query->fetch();
            if ($row) {
                if ($inputpassword == $password) {
                    $_SESSION["userid"] = $row['Id'];
                    $_SESSION["user"] = $row;
                    header("location: http://localhost/warsztat/mainpage.html");
                    exit;
                } else {
                    $error .= '<p class="error">Hasło nie jest poprawne</p>';
                }
            } else {
                $error .= '<p class="error">Nie znaleziono takiego użytkownika</p>';
            }
        }
        $query->close();
    }
    mysqli_close($db);
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Logowanie</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Logowanie</h2>
                    <p>Podaj dane aby się zalogować</p>
                    <form action="" method="post">
                        <div class="form-group">
                            <label for="username">Nazwa użytkownika</label>
                            <input type="text" name="username" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Hasło</label>
                            <input type="password" name="password" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-primary" value="Zaloguj">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>