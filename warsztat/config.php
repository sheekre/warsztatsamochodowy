<?php
define('DBSERVER', 'localhost');
define('DBUSERNAME', 'root');
define('DBPASSWORD', '');
define('DBNAME', 'warsztatsamochodowydb');

$db = mysqli_connect(DBSERVER, DBUSERNAME, DBPASSWORD, DBNAME);

if($db == false) {
    die("Błąd w połączeniu z bazą danych. " . mysqli_connect_error());
}
?>